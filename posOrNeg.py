import os
import re
import sys

#requires the scraped comments to be in the Comments/ folder
commentPath = "./Comments/"
#requires the Data/ folder to store the .csv files
dataPath = "./Data/"
dataFiles = os.listdir(dataPath)
files = os.listdir(commentPath)
for f in files:
   if not f.replace('.txt', '.csv') in dataFiles:
      doFile = raw_input("Perform tagging for " + f + "? [y/n]\n")
      if doFile == "y":
         regex = re.compile(r'".*?"', re.DOTALL)
         commentFile = open(commentPath + f, 'r')
         comments = regex.findall(commentFile.read())
         commentFile.close()
         for comment in comments:
            #combines in to one line for easier parsing
            comment = comment.replace("\n", " ")
            print comment
            rating = raw_input("1 - Positive | 2 - Negative | 3 - Critical/Neutral\n")

            #names the file <filename>.csv
            dataFileName = f.replace(".txt", ".csv")

            #creates file if it doesn't exist
            if not os.path.isfile(dataPath + dataFileName):
               create = open(dataPath + dataFileName, "w")
               create.close()

            #re-writing files in place
            os.rename(dataPath + dataFileName, dataPath + dataFileName + "~")
            dest = open(dataPath + dataFileName, "w")
            source = open(dataPath + dataFileName + "~", "r")

            found = False
            for line in source:
               if comment in line:
                  #adds new rating to end of line
                    dest.write(line.replace("\n","") + "," + rating + "\n")
                    found = True
               else:
                  dest.write(line)

            if not found:
               #creates new comment in data file
                dest.write(comment + "," + rating + "\n")

            source.close()
            os.remove(dataPath + dataFileName + "~")
            dest.close()

            #dataFile = open(dataPath + dataFileName, "w+")

