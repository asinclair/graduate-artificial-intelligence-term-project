import os 
import pprint
 
def parseCSV(path):
    files = os.listdir(path) 
     
    ratings = dict() 
     
    for f in files: 
#        print 'in file', f
        cfile = open(path + f, 'r')
        comments = cfile.read().split('\n')
        cfile.close()
        for c in comments:
            if len(c) > 0:
#               print 'comment: ', c
               firstQuote = c.find('"')
               lastQuote = c.rfind('"')
               scorings = c[lastQuote + 2:]
               comment = c[firstQuote + 1:lastQuote]
               ratings[comment] = int(scorings)
               #print 'scorings: ', scorings
               #if len(scorings) > 0:
                  #numbers = [int(x) for x in scorings.split(',')]
#                  print 'numbers: ', numbers
                  #comment = c[firstQuote + 1:lastQuote]
#                  print 'comment:', comment
                  #avg = int(round(sum(numbers) / float(len(numbers))))
                  # Handles duplicate comments
                  #if comment in ratings:
                     #ratings[comment] = int(round((ratings[comment]+avg)/2))
                  #else:
                     #ratings[comment] = avg 
    things = ratingIntsToStrings(ratings)
    #pprint.pprint(things)
    return things

# Replaces 1,2,3 with positive, negative, neutral
def ratingIntsToStrings(ratings):
   mapping = ["", "positive", "negative", "negative"]

   return dict((comment, mapping[rating]) for (comment, rating) in ratings.iteritems())

def parseLexicon():
    lexicon = dict()
    lexiconFile = open('lexicon.txt', 'r')
    words = lexiconFile.read().split('\n')
    for word in words:
        word = word.split(',')
        if len(word) == 3:
            if word[0] in lexicon:
                lexicon[word[0]] = (int(word[2]), lexicon[word[0]][1]) 
            else:
                lexicon[word[0]] = ('',int(word[2]))
    lexiconFile.close()

    return lexicon
