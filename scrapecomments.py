import soundcloud

import soundcloudlogin as scl
import unicodedata
import random

def writeCommentsToFile(track, client):
   trackComments = client.get('/tracks/%d/comments' % track.id, limit=200)
  
   fixedFilename = "Comments/Track" + str(track.id) + ".txt"
   outfile = open(fixedFilename, "w")
   # Uncomment line below to include track title in the file
   #title = unicodedata.normalize('NFKD', track.title).encode('ascii', 'ignore')
   #outfile.write(title + "\n")

   for comment in trackComments:
      asciicomment = unicodedata.normalize('NFKD', comment.body).encode('ascii', 'ignore')
      outfile.write("\"" + asciicomment + "\"")
      outfile.write("\n")
   outfile.close()

def scrapeRandomTracks(number):
   client = scl.login()
   savedTracks = []
   while len(savedTracks) < number:
      songs = []
      for s in savedTracks:
         songs.append(s.id)
      for i in range(number - len(savedTracks)):
         songs.append(random.randrange(scl.maxSongId))
      commaSep = ''
      for songID in songs:
         commaSep += str(songID) + ','
      commaSep = commaSep[:-1]

      tracks = client.get('/tracks', order='hotness', ids=commaSep)
      for track in tracks:
         if track.id not in [s.id for s in savedTracks]:
            trackComments = client.get('/tracks/%d/comments' % track.id, limit=50)
            if len(trackComments) == 50:
               savedTracks.append(track)
   return savedTracks

def generateTestSets(number):
   prefix = 'TestGroups/TestRun'
   suffix = '.txt'
   for i in range(167, 167 + number):
      print 'generating testset', i
      songs = scrapeRandomTracks(5)

      outfile = open(prefix + str(i) + suffix, 'w')
      for j in range(len(songs)):
         print 'writing', songs[j].id, 'to file from index', j
         outfile.write(str(songs[j].id))
         if j != len(songs) - 1:
            print j, 'is not', len(songs) - 1, 'so, writing comma'
            outfile.write(',')
      outfile.close()
   print 'writing complete'

def scrapeHottestTracks():
   client = scl.login() 
   tracks = client.get('/tracks', order='hotness', limit=100)

   for track in tracks:
      writeCommentsToFile(track, client)

if __name__ == '__main__':
   #scrapeHottestTracks()
   generateTestSets(10)
#   tracks = scrapeRandomTracks(10)
#   print 'Done getting tracks'
#   for track in tracks:
#      writeCommentsToFile(track, scl.login())
