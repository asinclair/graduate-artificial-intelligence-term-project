import itertools as it
import string
import nltk
import re
from collections import defaultdict
import csvTools as csv

import pprint

allFeatures = ['Unigram', 'Bigram', 'URL', 'Repeated', 'Exclaim', 'Sentiment', 'Length']
usedFeatures = ['URL', 'Repeated', 'Exclaim', 'Sentiment', 'Length']
  
# Feature Functions
def lexicon_features(document):
    features = {}
    positives = getSentimentWords(document, 'positive')
    negatives = getSentimentWords(document, 'negative')
    #if positives > 0 or negatives > 0:
     #   if positives >= negatives:
     #       features['overall'] = float(positives) / float((positives + negatives))
     #       features['more'] = 'positive'
     #   else:
     #       features['overall'] = float(negatives) / float((positives + negatives))
     #       features['more'] = 'negative'
    #else:
     #   features['overall'] = 0
     #   features['more'] = 'neutral'
    if positives > 0:
        features['num_pos'] = positives
    if negatives > 0:
        features['num_neg'] = negatives
    features['difference'] = positives - negatives
    weighted = (positives - negatives) * len(document)
    features['weighted_diff'] = weighted 
#    if positives > negatives:
#        features['more'] = 'positive'
#    if negatives > positives:
#        features['more'] = 'negative'
#    else:
#        features['more'] = 'negative'
    return features

def length_features(document):
    return {'length' : len(document)}

def repeated_features(document):
    repeated = getRepeatedLetters(document)
    if len(repeated) > 0:
        lengths = [len(d) for d in repeated]
        maxRepeat = max(lengths)
    else:
        maxRepeat = 0
    return {'highest_repeated' : maxRepeat} 

def exclaim_features(document):
    return {'exclaims' : len(getExclaimationPoints(document))} 

def url_features(document):
    return {'containsUrl' : hasUrl(document)}

def unigram_features(document, all_words):
   document_words = set(document.lower().split())
   features = {}
   for word in all_words:
      if word not in nltk.corpus.stopwords.words('english') and not word.startswith('@'):
         word = cleanWord(word)
         if word in document_words:
      #features['contains(%s)' % word] = (word in all_words)
            features['contains(%s)' % word] = True
   return features

def bigram_features(document, all_bigrams):
   document_bigrams = set(nltk.util.bigrams(document))
   features = {}
   for bigram in all_bigrams:
      features['contains(%s)' % (bigram,)] = (bigram in document_bigrams)
   return features

def stripRepeatedLetters(word):
   length = len(word) 
   i = 0
   while i < length:
      if i > 0:
         if word[i] == word[i - 1]:
            word = word[:i-1] + word[i:]
            length = length - 1
      i = i + 1
   return word

# Helper Functions
def hasRepeatedChars(text):
    repeated = getRepeatedLetters(text)
    if len(repeated) > 0:
        return True
    else:
        return False

def hasRepeatedExcl(text):
    excl = getExclaimationPoints(text)
    if len(excl) > 1:
        return True
    else:
        return False 

# Utility Functions
def getSentimentWords(document, flag):
    lexicon = csv.parseLexicon()
    total = 0
    for word in document.split():
        word = cleanWord(word)
        if word in lexicon:
            if flag == "positive":
                total += lexicon[word][0]
            else:
                total += lexicon[word][1]
    return total

def getRepeatedLetters(text):
    text = text.replace(' ', '')
    repeated = [list(g) for k, g in it.groupby(text)]
    #repeated = [r for r in repeated if len(r) > 22]
    return repeated

def getExclaimationPoints(text):
    text = text.replace(' ', '')
    excl = [t for t in text if t == '!']
    return excl

def isUrl(text):
   urlPattern = '''^((?:[a-z][\w-]+:(?:/{1,3}|[a-z0-9%])|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:'".,<>?""'']))$'''
   qpattern = re.compile(urlPattern)
   result = qpattern.match(text)
   if result:
      return True;
   else:
      return False;

def hasUrl(text):
   urlPattern = '''((?:[a-z][\w-]+:(?:/{1,3}|[a-z0-9%])|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:'".,<>?""'']))'''
   qpattern = re.compile(urlPattern)
   result = qpattern.match(text)
   if result:
      return True;
   else:
      return False;

# lowers the case, removes duplicate letters, removes punctuation
def cleanWord(word):
   word = stripRepeatedLetters(word.lower())
   exclude = set(string.punctuation)
   stripped = ''.join(ch for ch in word if ch not in exclude)

   return stripped

# documents = list of comments
def getAllTrainingWords(documents):
   words = []
   for doc in documents:
      words += stripPunctuation(doc[0])
   d = defaultdict(int)
   for word in words:
      #d[word.lower()] += 1
      d[stripRepeatedLetters(word.lower())] += 1

   # Return only words that appear more than once 
   return [word for word in d if d[word] > 2 and len(word) > 1]

def stripPunctuation(document):
   stripped = []
   exclude = set(string.punctuation)
   for word in document.split():
      stripped.append(''.join(ch for ch in word if ch not in exclude))
   return stripped

def getAllBigrams(documents):
   bigrams = []
   for doc in documents:
      bigrams += nltk.util.bigrams(stripPunctuation(doc[0]))
   return set(bigrams)

# For Brute Force Testing
def getAllCombos(feature_sets, name_sets):
   all_feature_sets = []
   all_name_sets = []
   all_zipped = {}
   # Change the range to change the number of features you want together.
   # i.e. range(3) gives you up to 2 features together
   for i in range(3):
      for subset in list(it.combinations(feature_sets, i+1)):
         all_feature_sets.append(subset)
      for subset in list(it.combinations(name_sets, i+1)):
         all_name_sets.append(subset)
   all_zipped = dict(zip(all_name_sets, all_feature_sets))
   for k, subset in all_zipped.items():
      all_zipped[k] = list(it.chain(*subset))
   return all_zipped

def generate_set(parsed, features, all_words):
    feature_sets = []
    #Unigrams
    if allFeatures[0] in features:
        feature_sets.append([(unigram_features(c, all_words), r) for (c, r) in parsed])
    if allFeatures[1] in features:
        all_bigrams = getAllBigrams(parsed)
        feature_sets.append([(bigram_features(c, all_bigrams), r) for (c, r) in parsed])
    if allFeatures[2] in features:
        feature_sets.append([(url_features(c), r) for (c, r) in parsed])
    if allFeatures[3] in features:
        feature_sets.append([(repeated_features(c), r) for (c, r) in parsed])
    if allFeatures[4] in features:
        feature_sets.append([(exclaim_features(c), r) for (c, r) in parsed])
    if allFeatures[5] in features:
        feature_sets.extend([(lexicon_features(c), r) for (c, r) in parsed])
    if allFeatures[6] in features:
        feature_sets.extend([(length_features(c), r) for (c, r) in parsed])

    print 'feature_sets is of length:', len(feature_sets)
    allCombos = getAllCombos(feature_sets, features)
    #pprint.pprint(allCombos)
    return allCombos 


def generate_featureset(parsed, features, all_words):
    feature_sets = []
    
    #Unigrams
    if allFeatures[0] in features:
        feature_sets.extend([(unigram_features(c, all_words), r) for (c, r) in parsed])
    if allFeatures[1] in features:
        all_bigrams = getAllBigrams(parsed)
        feature_sets.extend([(bigram_features(c, all_bigrams), r) for (c, r) in parsed])
    if allFeatures[2] in features:
        feature_sets.extend([(url_features(c), r) for (c, r) in parsed])
    if allFeatures[3] in features:
        feature_sets.extend([(repeated_features(c), r) for (c, r) in parsed])
    if allFeatures[4] in features:
        feature_sets.extend([(exclaim_features(c), r) for (c, r) in parsed])
    if allFeatures[5] in features:
        feature_sets.extend([(lexicon_features(c), r) for (c, r) in parsed])
    if allFeatures[6] in features:
        feature_sets.extend([(length_features(c), r) for (c, r) in parsed])

    return feature_sets


def extractFeaturesFromComment(comment, features, all_words):
   commentfeatures = dict()

   if allFeatures[0] in features:
      commentfeatures.update(unigram_features(comment, all_words))
   if allFeatures[2] in features:
      commentfeatures.update(url_features(comment))
   if allFeatures[3] in features:
      commentfeatures.update(repeated_features(comment))
   if allFeatures[4] in features:
      commentfeatures.update(exclaim_features(comment))
   if allFeatures[5] in features:
      commentfeatures.update(lexicon_features(comment))
   if allFeatures[6] in features:
      commentfeatures.update(length_features(comment))

   return commentfeatures
