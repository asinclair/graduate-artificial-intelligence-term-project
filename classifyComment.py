import nltk
import featureExtractionMethods as fem
import csvTools as ct

validationPath = "./Validation/"
trainingPath = "./Data/"

def generateTrainingSet():
    parsed = ct.parseCSV(trainingPath)
    return parsed.items()    

def generateValidationSet():
    parsed = ct.parseCSV(validationPath)
    return parsed.items()    

def getTrainingWords():
    training_set = generateTrainingSet()
    training_words = fem.getAllTrainingWords(training_set)
    return training_words

def classify():
    training_set = generateTrainingSet()
    validation_set = generateValidationSet() 

    training_words = fem.getAllTrainingWords(training_set)
    # Specify which features to include
    features = fem.allFeatures

    t_feature_set = fem.generate_set(training_set, features, training_words)
    v_feature_set = fem.generate_set(validation_set, features, training_words)

    for k, subset in t_feature_set.iteritems():
        print "Calculating", "|".join(k)
        classifier = nltk.NaiveBayesClassifier.train(subset)
        print "Accuracy:", nltk.classify.accuracy(classifier, v_feature_set[k])

    return classifier

def trainClassifier():
    features = fem.usedFeatures
    
    training_set = generateTrainingSet()
    validation_set = generateValidationSet()
    training_words = fem.getAllTrainingWords(training_set)

    t_feature_set = fem.generate_featureset(training_set, features, training_words)
    v_feature_set = fem.generate_featureset(training_set, features, training_words)

    classifier = nltk.NaiveBayesClassifier.train(t_feature_set)
#    print "Accuracy:", nltk.classify.accuracy(classifier, v_feature_set) 
    return classifier 



