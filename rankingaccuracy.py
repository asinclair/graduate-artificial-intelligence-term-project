import math

def calculateRankAccuracy(trueOrder, calculatedOrder):
   rankDifferenceTotal = 0
   for calculatedIndex, item in enumerate(calculatedOrder):
      rankDifferenceTotal += rankDistance(trueOrder.index(item), calculatedIndex)
   return rankDifferenceTotal 

def rankDistance(trueIndex, calculatedIndex):
   return math.fabs(trueIndex - calculatedIndex)
