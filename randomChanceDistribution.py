import itertools 
from collections import defaultdict
import rankingaccuracy as r
trueRank = [1, 2, 3, 4, 5]
import pprint

d = defaultdict(int)


for instance in itertools.permutations(trueRank, 5):
   d[r.calculateRankAccuracy(trueRank, instance)] += 1

pprint.pprint(d)




