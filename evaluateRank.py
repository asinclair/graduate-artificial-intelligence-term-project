import classifyComment as cc
from collections import defaultdict
import rankingaccuracy as ra
import featureExtractionMethods as fem
from random import shuffle

import pprint

def evaluateRank(tracksToEvaluate, classifier, training_words):
   trueRank = [id for (id, comments) in tracksToEvaluate]
#   print 'trueRank: ', trueRank
   calculatedRank = []


   trackCommentTallies = []
   myOrder = list(reversed(range(len(trueRank))))
   tracksToEvaluate = [tracksToEvaluate[i] for i in myOrder] 
#   print 'newRank: ', [id for (id, comments) in tracksToEvaluate]
   tracksToEvaluate.reverse()
   for track in tracksToEvaluate:
     trackCommentTallies.append((track[0], tallyCommentClasses(track[1], classifier, training_words)))
      
   calculatedRank = rankTalliedTracks(trackCommentTallies)
#   print "Calculated rank: " + str(calculatedRank)
   accuracy = ra.calculateRankAccuracy(trueRank, calculatedRank)
   return accuracy

# Returns a dictionary that maps classes to frequencies
def tallyCommentClasses(comments, classifier, unigrams):
   classTotals = defaultdict(int)
   classTotals["positive"] = 0
   classTotals["negative"] = 0
   classTotals["neutral"] = 0

   features = fem.usedFeatures

   for comment in comments:
      commentFeatures = fem.extractFeaturesFromComment(comment, features, unigrams) 
      classTotals[classifier.classify(commentFeatures)] += 1
#   pprint.pprint(classTotals)
   return classTotals

def rankTalliedTracks(trackTallies):
   # Returns list of track ids sorted into descending order of positive comment percentages
   return [id for (id, tallies) in sorted(trackTallies, key=lambda track: rankValue(track), reverse=True)]

      

# Returns percentage of positive comments
def rankValue(classTotals):
   totalComments = 0
   for key in classTotals[1].keys():

      totalComments += classTotals[1][key]

   return float(classTotals[1]["positive"] - classTotals[1]['negative']) 

   
