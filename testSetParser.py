from random import choice
import os 
import soundcloudlogin as scl
import classifyComment as cc
import featureExtractionMethods as fem
import evaluateRank as er

def queryRank(filename):
   infile = open(filename, 'r')
   commaSep = infile.read()
   infile.close()

   return scl.login().get('/tracks', order='hotness', ids=commaSep) 

# This function can receive either a list of trackIDs, or a list of tracks,
# as defined by the soundcloud module
# Returns a list of tuples of the form (trackID, comments[])
def getComments(trackList):
   if len(trackList) <= 0:
      return
   returnTuples = []
   usingIDs = type(trackList[0]) == int

   for track in trackList:
      idVal = 0
      if usingIDs:
         idVal = track
      else:
         idVal = track.id
      comments = [c.body for c in scl.login().get('/tracks/%d/comments' % idVal, limit=100)]
      returnTuples.append((idVal, comments))

   return returnTuples

# Returns a list where each entry is a list of tuples (as returned by getComments)
def getAllTestGroups():
   listOfListsOfTuples = []
   directory = 'TestGroups'
   for f in os.listdir(directory):
      testSet = queryRank(directory + '/' + f)
      listOfListsOfTuples.append(getComments(testSet))

   return listOfListsOfTuples

def rankAllTestGroups():
    classifier = cc.trainClassifier()
#    classifier.show_most_informative_features(20)
    training_words = cc.getTrainingWords()

    for f in sorted(os.listdir('TestGroups')):
      print er.evaluateRank(getRankedList(f), classifier, training_words)

# Will randomly select a test set in TestGroups, parse it, and provide the tuple-list
def getRandomRankedList():
   selectedFile = choice(os.listdir('TestGroups'))
   return getRankedList(selectedFile)

# Will used a specified test set in TestGroups, parse it, and provide the tuple-list
def getSpecificRankedList(number):
   name = 'TestRun' + str(number) + '.txt'
   return getRankedList(name)

# Uses the name of a file in TestGroups, and returns the tuple-list
def getRankedList(filename):
   sys.stderr.write('using %s' % filename)

   ranked = [track.id for track in queryRank('TestGroups/' + filename)]
   tupleList = getComments(ranked)
#for tup in tupleList:
#     print tup[0], ':'
#      for com in tup[1]:
#         print com
   return tupleList

rankAllTestGroups()
#classifier = cc.trainClassifier()
#trainingwords = cc.getTrainingWords()
#print er.evaluateRank(getSpecificRankedList(155), classifier, trainingwords)
