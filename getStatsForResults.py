import operator 
import os
import math
import sys

def getScores(infile):
   out = open(infile, 'r')
   text = out.read()
   lines = text.split('\n')
   scores = [float(score) for score in lines if score and not score.startswith('using')]
   return scores

def doStats(scores):
   count = len(scores)
   print 'Count:\t\t', count 
   mean = sum(scores) / float(count)
   print 'Mean:\t\t', mean 

   sum_x2 = 0
   freqs = {}
   for score in scores:
      sum_x2 += score * score
      if score in freqs.keys():
         freqs[score] += 1
      else :
         freqs[score] = 1
   stddev = math.sqrt((sum_x2 / float(count)) - (mean * mean))
   print 'Std. Dev.:\t', stddev

   print 'Frequencies:\t', freqs

   sort = sorted(freqs.iteritems(), key=operator.itemgetter(1), reverse=True)
   mode = sort[0][0]
   print 'Mode:\t\t', mode

infile = sys.stdin.readline().replace('\n', '')
print infile
doStats(getScores(infile))
