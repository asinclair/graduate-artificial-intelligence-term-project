import soundcloud
import soundcloudlogin as scl
import scrapecomments as sc
import sys

if len(sys.argv) <= 1 or sys.argv[1] == "":
   print "Usage: python scrapesingle.py SoundCloudURL"
   exit()

client = scl.login()
track = client.get('/resolve', url=sys.argv[1])

sc.writeCommentsToFile(track, client)
